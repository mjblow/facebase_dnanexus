#FaceBase_DNANexus
Scripts for running analysis of Facebase data on DNANexus

###Step 1. Setup environment
	source activate facebase
  
####facebase setup
Make sure that you have a login key stored in .deriva/credential.json  
Alternatively:generate a temporaryFACEBASE login key:  
run mac app DERIVA-Auth and copy the token (see https://github.com/informatics-isi-edu/facebase-curation/wiki/Bulk-Download)  
  
####dnanexus setup
Verify that you are logged in to DNAnexus command line interface:  
  
	dx ls  
    
If not logged in, generate a DNAnexus authentication token:  
Log in to DNAnexus website and create token (see https://wiki.dnanexus.com/Command-Line-Client/Login-and-Logout#Authentication-Tokens)  
Log in to command line interface:  
  
	dx login --token <dx_token> --save  
  
###Step 2. Download data from FaceBase
	perl facebase_download.pl <FaceBase_accession> <DERIVA-Auth token>

###Step 3. Upload data to DNA Nexus (run on dtn02)
	perl dx_uploader.pl <FaceBase_accession>

###Step 4. Setup ChIP-Seq analyses for accession
	perl dx_chipseq_analysis.pl <FaceBase_accession>

###Step 5. Setup RNA-Seq analyses for accession
	perl dx_rnaseq_analysis.pl <FaceBase_accession>

###Step 6. Submit the analyses to DNANexus
	sh <FaceBase_accession>.dx_submit.sh

###Step 7. Download the results from DNANexus (run on dtn02)
	sh <FaceBase_accession>.dx_results_download.sh

###Step 8. Upload the results to FaceBase (run on dtn02)
	e.g. ~/.conda/envs/facebase/bin/deriva-upload-cli --token=eq8C4b4DF2wpZ8tKoc6zmgbY staging.facebase.org /projectb/scratch/mjblow/facebase_dnanexus/data/FB00000806.2
	e.g. ~/.conda/envs/facebase/bin/deriva-upload-cli --credential-file=~/.deriva/credential.json staging.facebase.org /projectb/scratch/mjblow/facebase_dnanexus/data/FB00000806.2
