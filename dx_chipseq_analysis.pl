#!/usr/bin/env perl
##!/global/projectb/sandbox/RD/mjblow/envs/facebase/bin/perl
use warnings;
use strict;
#use lib '/global/projectb/sandbox/RD/mjblow/envs/facebase/lib/perl5/site_perl/5.22.0/x86_64-linux-thread-multi/';
use File::Basename;
use JSON::Parse 'json_file_to_perl';
use Data::Dumper;
use List::Uniq ':all';


#source activate facebase
#must be logged in to DNANexus using an API token

unless(@ARGV == 3){
	die "$0 <FaceBase_accession> <results_path> <server>\n";
}
my $accession = $ARGV[0];
my $path = $ARGV[1];
my $server = $ARGV[2];

my $pipeline_rid;

if($server eq "staging.facebase.org"){
	$pipeline_rid = "1-3YJC";
}elsif($server eq "www.facebase.org"){
	$pipeline_rid = "1-3YNY";
}else{
	die "server $server not valid!\n$!\n";
}

#Check that we have the input data jsons
my $local_path = "$path/data/$accession";
my $json = "$path/json/$accession-ChIP-Seq.json";
unless(-f("$json")){
	die "Can't find json file for $accession in $path/json/. Check data download from FaceBase\n";
}

#Write dna nexus job submission commands to a file
my $dx_submit = "$accession.chipseq.dx_submit.sh";
open(my $cmd_fh, ">$dx_submit") or die $!;

#Write dna nexus results download to another file
my $dx_download = "$accession.chipseq.dx_results_download.sh";
open(my $download_fh, ">$dx_download") or die $!;
print $download_fh "bin/dx select project-FF7xxXQ0jK8Fq5Kf4XzvFKf8\n\n";
print $download_fh "mkdir -p $local_path\n"; 
print $download_fh "dx download --no-progress -f -r -o $local_path $accession/ChIP-seq\n\n"; 

#ENCODE Uniform Processing Pipelines/ChIP-seq/workflows
#project-BKpvFg00VBPV975PgJ6Q03v6 (use dx describe project-BKpvFg00VBPV975PgJ6Q03v6 for details)
my $histone_r1_c1_workflow = "workflow-FFBQv4j0jK87xQzX0vF6gBYy"; #ENCODE histone ChIP-seq Unary Control Unreplicated (specify reference) - workflow-F7KQY800VBPxvJ6y0ZZzKpYG
my $tf_r1_c1_workflow = "workflow-FFBQv4j0jK87xQzX0vF6gBZB"; #ENCODE TF ChIP-seq Unary Control Unreplicated (specify reference) - workflow-F7KQY2j0VBPxvJ6y0ZZzKpY6

#ENCODE reference file ids
my $hg19_genome = "project-BKpvFg00VBPV975PgJ6Q03v6:file-BYjVz8Q0Qy56Jq7vZJGzqVzP"; 
my $hg19_chrom_sizes = "project-BKpvFg00VBPV975PgJ6Q03v6:file-BP4pGg00Qy57pYPx35BQ02f3";
my $mm10_genome = "project-BKpvFg00VBPV975PgJ6Q03v6:file-BpVX6P80Qy511G1650qZVGjj"; 
my $mm10_chrom_sizes = "project-BKpvFg00VBPV975PgJ6Q03v6:file-Bv77qQ00Qy5JK0kVkVxJ83q3";

#iterate through epxeriments for this accession
my ($experiments) = read_json($json);
my @experiments = @{$experiments};

#print the number of chip-seq experiments
print scalar(@experiments);

foreach my $exp(@experiments){
	#FB00000806.2, 51273, 51261, Mus musculus, Histone modification, Single-end, 1, LPHW_101514_001B_e11.5_face_md_H3K27ac.fastq.gz, NA, NA, NA, 1, LPHW_101514_001A_e11.5_face_md_in_combined.fastq.gz, NA, NA, NA
	my($acc, $ex_id, $replicate, $ctrl_replicate, $species, $exp_type, $paired, $fq_c1_r1, $fq_c1_r2, $fq_i1_r1, $fq_i1_r2) = @{$exp};

	my $c1r1_id = "FaceBase:/$acc/fastq/$fq_c1_r1";
       	my $c1r2_id = "FaceBase:/$acc/fastq/$fq_c1_r2";
	my $i1r1_id = "FaceBase:/$acc/fastq/$fq_i1_r1"; 
	my $i1r2_id = "FaceBase:/$acc/fastq/$fq_i1_r2"; 

	#Reference genome id and related
	my($genome, $chrom_sizes, $genome_size, $genome_dir);

	if($species eq "Homo sapiens"){
		$genome = $hg19_genome;
		$chrom_sizes = $hg19_chrom_sizes;
		$genome_size = "hs";
		$genome_dir = "hg19"
	}elsif($species eq "Mus musculus"){
		$genome = $mm10_genome;
		$chrom_sizes = $mm10_chrom_sizes;
		$genome_size = "mm";
		$genome_dir = "mm10"
	}else{
		die "species $species is not supported\n";
	}
	
	#Setup the analysis directory
	my $workflow;
	my $cmd;
	my $results_path = "FaceBase:/$acc/ChIP-seq/$pipeline_rid/$replicate/proc/$genome_dir";
	$cmd = "dx mkdir -p $results_path; ";
	my $analysis_name = "$ex_id\.$replicate-analysis";

	#check ifa successful analysis with this name already exists
	#my $analysis_test = `dx find analyses --name $analysis_name --state done`;
	#unless($analysis_test eq ""){
	#	$cmd = "#$cmd";	
	#	print "Analysis already exists for $analysis_name! Edit $dx_submit to rerun it.\n";
	#}

	my $tagline = "--tag ChIP-seq --tag dataset=$acc --tag $paired --tag replicate=$replicate --tag experiment=$ex_id --tag \"$species\" ";

	#Histone ChIP-Seq
	if($exp_type eq "Histone modification"){
		#select histone unreplicatede workflow
		$workflow = $histone_r1_c1_workflow; #ENCODE histone ChIP-seq Unary Control Unreplicated (specify reference)
		#build command	
		#Single-end
		if($paired eq "Paired-end"){
			$cmd .= "dx run --brief $workflow --yes --destination=$results_path --name=$analysis_name $tagline".
			"-i0.reads1=$c1r1_id ".
			"-i0.reads2=$c1r2_id ".
			"-i0.reference_tar=$genome ".
			"-i1.reads1=$i1r1_id ".
			"-i1.reads2=$i1r2_id ".
			"-i6.chrom_sizes=$chrom_sizes ".
			"-i6.genomesize=$genome_size";
		}elsif($paired eq "Single-end"){
			$cmd .= "dx run --brief $workflow --yes --destination=$results_path --name=$analysis_name $tagline".
			"-i0.reads1=$c1r1_id ".
			"-i0.reference_tar=$genome ".
			"-i1.reads1=$i1r1_id ".
			"-i6.chrom_sizes=$chrom_sizes ".
			"-i6.genomesize=$genome_size";
		}
	}
	elsif($exp_type eq "Transcription factor"){
		#ENCODE TF ChIP-seq Unary Control Unreplicated (specify reference) workflow-F7KQY2j0VBPxvJ6y0ZZzKpY6 
		$workflow = $tf_r1_c1_workflow;
		if($paired eq "Paired-end"){
			$cmd .= "dx run --brief $workflow --yes --destination=$results_path --name=$analysis_name $tagline".
				"-i0.reads1=$c1r1_id ".
				"-i0.reads2=$c1r2_id ".
				"-i0.reference_tar=$genome ".
				"-i1.reads1=$i1r1_id ".
				"-i1.reads2=$i1r2_id ".
				"-i6.chrom_sizes=$chrom_sizes ".
				"-i6.genomesize=$genome_size";
		}elsif($paired eq "Single-end"){
			$cmd .= 	"dx run --brief $workflow --yes --destination=$results_path --name=$analysis_name $tagline".
				"-i0.reads1=$c1r1_id ".
				"-i0.reference_tar=$genome ".
				"-i1.reads1=$i1r1_id ".
				"-i6.chrom_sizes=$chrom_sizes ".
				"-i6.genomesize=$genome_size";
		}
	}else{
		die "Dont know what to do with experiment type: $exp_type!\n";
	}
	print $cmd_fh "$cmd\n";
	#my $run = `$cmd`;
	
	#Setup for data download (dnanexus->LBL) and renaming / restructuring
	#flatten directory structure
	my $exp_path = "$local_path/ChIP-seq/$pipeline_rid/$replicate/proc/$genome_dir";
	print $download_fh "#Experiment=$ex_id Replicate=$replicate\n";
	print $download_fh "mv $exp_path/encode_macs2/* $exp_path\n";
	print $download_fh "mv $exp_path/encode_map/* $exp_path\n";

	#rename coverage files
	print $download_fh "mv $exp_path/final.replicated.broadPeak.bb $exp_path/$replicate.broadPeak.bb\n";
	print $download_fh "mv $exp_path/final.replicated.broadPeak.gz $exp_path/$replicate.broadPeak.gz\n";
	print $download_fh "mv $exp_path/final.replicated.gappedPeak.bb $exp_path/$replicate.gappedPeak.bb\n";
	print $download_fh "mv $exp_path/final.replicated.gappedPeak.gz $exp_path/$replicate.gappedPeak.gz\n";
	print $download_fh "mv $exp_path/final.replicated.narrowPeak.bb $exp_path/$replicate.narrowPeak.bb\n";
	print $download_fh "mv $exp_path/final.replicated.narrowPeak.gz $exp_path/$replicate.narrowPeak.gz\n";
	print $download_fh "mv $exp_path/r1.fc_signal.bw	$exp_path/$replicate.fc_signal.bw\n";
	print $download_fh "mv $exp_path/r1.pvalue_signal.bw $exp_path/$replicate.pvalue_signal.bw\n";
	
	#rename alignment files
	my $fq_c1_r1_base = basename($fq_c1_r1); 
	$fq_c1_r1_base =~ s/\.fastq.gz//;
	my $fq_c1_r2_base = basename($fq_c1_r2);
	$fq_c1_r2_base =~ s/\.fastq.gz//;
	my $fq_i1_r1_base = basename($fq_i1_r1); 
	$fq_i1_r1_base =~ s/\.fastq.gz//;
	my $fq_i1_r2_base = basename($fq_i1_r2);
	$fq_i1_r2_base =~ s/\.fastq.gz//;
	my ($chip_prefix, $control_prefix);
	if($fq_c1_r1_base ne "NA" && $fq_c1_r2_base ne "NA"){
		$chip_prefix = $fq_c1_r1_base."_".$fq_c1_r2_base;
	}else{
		$chip_prefix = $fq_c1_r1_base;
	}
	if($fq_i1_r1_base ne "NA" && $fq_i1_r2_base ne "NA"){
		$control_prefix = $fq_i1_r1_base."_".$fq_i1_r2_base;
	}else{
		$control_prefix = $fq_i1_r1_base;
	}
	print $download_fh "mv $exp_path/$chip_prefix.raw.srt.bam $exp_path/$replicate.unfiltered.bam\n";
	print $download_fh "mv $exp_path/$control_prefix.raw.srt.bam $exp_path/$ctrl_replicate.unfiltered.bam\n";
	print $download_fh "mv $exp_path/$chip_prefix.raw.srt.filt.nodup.srt.bam $exp_path/$replicate.filtered.bam\n";
	print $download_fh "mv $exp_path/$control_prefix.raw.srt.filt.nodup.srt.bam $exp_path/$ctrl_replicate.filtered.bam\n";
	
	#move control bams to their own folder
	my $control_path = "$local_path/ChIP-seq/$pipeline_rid/$ctrl_replicate/proc/$genome_dir";
	print $download_fh "mkdir -p $control_path\n"; 
	print $download_fh "mv -n $exp_path/$ctrl_replicate*bam $control_path/.\n";
        print $download_fh "rm -rf $exp_path/$ctrl_replicate* $exp_path/$chip_prefix* $exp_path/$control_prefix* $exp_path/encode* $exp_path/r1* $exp_path/final*\n"; 
	print $download_fh "\n";
}

#login to dnanexus
#system("dx logout") == 0 or die $!;

sub read_json{
	my ($chip_seq_json) = @_;
	my @chip_seq_args;

	#read json as array of hashes. One array element = a fastq with associated metadata
	my $all_metadata = json_file_to_perl ($chip_seq_json);
	my @all_metadata = @{$all_metadata};

	#Get a unique list of ChIP replicate IDs
	my @chips  = grep {$_->{target} ne 'Control'} @all_metadata;
	my @chip_replicates = uniq(map {$_->{replicate}} @chips);

	foreach my $replicate(@chip_replicates){
		#get the json elements for each fastq for this replicate (info is duplicated apart from read number and filename)
		my ($fastq1_dat, $fastq1_file) = parse_fastq($replicate, 1, \@all_metadata);
		my ($fastq2_dat, $fastq2_file) = parse_fastq($replicate, 2, \@all_metadata);
		my %fastq1_dat = %{$fastq1_dat};
		#
		#get corresponding control replicate id(s)
		#currently can only handle a single replicate
		#Select the first replicate in case of more than one
		my $control = $fastq1_dat{control};
		my @control_replicates =uniq( map {$_->{replicate}} grep {$_->{experiment} eq $control} @all_metadata);
		my $control_replicate = $control_replicates[0];

		#get the json elements for each control fastq
		my ($ctrl1_dat, $ctrl1_file) = parse_fastq($control_replicate, 1, \@all_metadata);
		my ($ctrl2_dat, $ctrl2_file) = parse_fastq($control_replicate, 2, \@all_metadata);

		my @args = ($fastq1_dat{dataset}, $fastq1_dat{experiment}, $replicate, $control_replicate, $fastq1_dat{species}, $fastq1_dat{target}, $fastq1_dat{paired},$fastq1_file, $fastq2_file, $ctrl1_file, $ctrl2_file);
                push(@chip_seq_args, \@args);
		
	}return \@chip_seq_args;

}

sub parse_fastq{
	my ($replicate, $strand, $metadata) = @_;
	my @all_metadata = @{$metadata};
	my (%fastq_dat, $fastq_file);
	my @fastq = grep {$_->{replicate} eq $replicate && $_->{read} == $strand} @all_metadata;
	if(@fastq > 0){
		%fastq_dat = %{$fastq[0]};		
		$fastq_file = $fastq_dat{filename};
	}else{
		$fastq_file = "NA";
	}
	return(\%fastq_dat, $fastq_file);
}
