#!/usr/bin/env perl
use warnings;
use strict;
use File::Basename;
use JSON::Parse 'json_file_to_perl';
use Data::Dumper;
use List::Uniq ':all';


#source activate facebase
#must be logged in to DNANexus using an API token

unless(@ARGV == 3){
        die "$0 <FaceBase_accession> <results_path> <server>\n";
}
my $accession = $ARGV[0];
my $path = $ARGV[1];
my $server = $ARGV[2];

my $pipeline_rid;

if($server eq "staging.facebase.org"){
        $pipeline_rid = "1-3YJC";
}elsif($server eq "www.facebase.org"){
        $pipeline_rid = "1-3YNY";
}else{
        die "server $server not valid!\n$!\n";
}


#Check that we have the input data jsons
my $local_path = "$path/data/$accession";
my $json = "$path/json/$accession-RNA-Seq.json";
unless(-f("$json")){
	die "Can't find json file for $accession in $path/json/. Check data download from FaceBase\n";
}

#Write dna nexus job submission commands to a file
my $dx_submit = "$accession.rnaseq.dx_submit.sh";
open(my $cmd_fh, ">$dx_submit") or die $!;

#Write dna nexus results download to another file
my $dx_download = "$accession.rnaseq.dx_results_download.sh";
open(my $download_fh, ">$dx_download") or die $!;
print $download_fh "dx select project-FF7xxXQ0jK8Fq5Kf4XzvFKf8\n\n";
print $download_fh "mkdir -p $local_path\n";
print $download_fh "dx download --no-progress -f -r -o $local_path $accession/RNA-seq\n\n";


#FaceBase:/long-RNA-seq
my $se_r1_workflow = "workflow-FFBQv4j0jK87xQzX0vF6gBPg"; #ENCODE RNA-Seq (Long) Pipeline - 1 (single-end) replicate
my $pe_r1_workflow = "workflow-FFBQv4j0jK87xQzX0vF6gBPx"; #ENCODE RNA-Seq (Long) Pipeline - 1 (paired-end) replicate

#ENCODE reference file ids
my $hg19_star_index = "project-BKpvFg00VBPV975PgJ6Q03v6:file-BZGykk00Q72fkFvf54vB0Zzj"; #hg19_male_v19_ERCC_starIndex.tgz 
my $hg19_chrom_sizes = "project-BKpvFg00VBPV975PgJ6Q03v6:file-BP4pGg00Qy57pYPx35BQ02f3"; #male.hg19.chrom.sizes
my $hg19_rsem_index = "project-BKpvFg00VBPV975PgJ6Q03v6:file-BV59gF80Bg5Y6qyGyvY37fXg"; #hg19_male_v19_ERCC_rsemIndex.tgz
my $mm10_star_index = "project-BKpvFg00VBPV975PgJ6Q03v6:file-BZGy9600P1JfFx5G0Zv7bzfj"; #mm10_male_M4_ERCC_starIndex.tgz
my $mm10_chrom_sizes = "project-BKpvFg00VBPV975PgJ6Q03v6:file-Bv77qQ00Qy5JK0kVkVxJ83q3"; #mm10_no_alt.chrom.sizes
my $mm10_rsem_index = "project-BKpvFg00VBPV975PgJ6Q03v6:file-BX3bGBj0FkxFVqpKyVjx070v"; #mm10_male_M4_ERCC_rsemIndex.tgz

#iterate through analyses for this accession
my ($analyses) = read_json($json);
my @analyses = @{$analyses};
print scalar(@analyses);
foreach my $analysis(@analyses){
	#FB00000806.2, 51273, 51261, Mus musculus, Histone modification, Single-end, 1, LPHW_101514_001B_e11.5_face_md_H3K27ac.fastq.gz, NA, NA, NA, 1, LPHW_101514_001A_e11.5_face_md_in_combined.fastq.gz, NA, NA, NA
	my($acc, $ex_id, $replicate, $species, $paired, $read_strand, $fq_r1_r1, $fq_r1_r2) = @{$analysis};

	#Paths to the input fastq files in DNAnexus
	my $r1r1_id = "FaceBase:/$acc/fastq/$fq_r1_r1";
       	my $r1r2_id = "FaceBase:/$acc/fastq/$fq_r1_r2";

	#Reference genome id and related
	my($star_index, $chrom_sizes, $rsem_index, $genome_dir);

	if($species eq "Homo sapiens"){
		$star_index = $hg19_star_index;
		$chrom_sizes = $hg19_chrom_sizes;
		$rsem_index = $hg19_rsem_index;
		$genome_dir = "hg19"
	}elsif($species eq "Mus musculus"){
		$star_index = $mm10_star_index;
		$chrom_sizes = $mm10_chrom_sizes;
		$rsem_index = $mm10_rsem_index;
		$genome_dir = "mm10";
	}else{
		die "species $species is not supported\n";
	}
	
	#Setup the analysis directory
	my $workflow;
	my $cmd;
	my $results_path = "FaceBase:/$acc/RNA-seq/$pipeline_rid/$replicate/proc/$genome_dir";
	$cmd = "dx mkdir -p $results_path; ";
	my $analysis_name = "$ex_id\.$replicate-analysis";

	#check if successful analysis with this name already exists
	#my $analysis_test = `dx find analyses --name $analysis_name --state done`;
	#unless($analysis_test eq ""){
	#	$cmd = "#$cmd";	
	#	print "Analysis already exists for $analysis_name! Edit $dx_submit to rerun it.\n";
	#}

	my $tagline = "--tag RNA-seq --tag dataset=$acc --tag $paired --tag replicate=$replicate --tag experiment=$ex_id --tag \"$species\" ";
	
	#sort out strandedness
	$read_strand = lc($read_strand);
	my $stranded = "false";
	if(($read_strand eq "forward") or ($read_strand eq "reverse")){
		$stranded = "true";
	}else{
		$read_strand = "unstranded";
	}
	#Read1 orientation (default: unstranded): [-istage-F8B6zPQ0VBPf8kqJG3BZKky9.read_strand=(string, default="unstranded")]
	#       Choices: reverse, -, TruSeq, forward, +, ScriptSeq, unstranded


	# One technical replicate
	if($paired eq "Paired-end"){
		#select histone unreplicatede workflow
		$workflow = $pe_r1_workflow;  #ENCODE RNA-Seq (Long) Pipeline - 1 (paired-end) replicate
		$cmd .= "dx run --brief $workflow --yes --destination=$results_path --name=$analysis_name $tagline".
		"-i0.reads1=$r1r1_id ".
		"-i0.reads2=$r1r2_id ".
		"-i0.star_index=$star_index ".
		"-i1.chrom_sizes=$chrom_sizes ".
		"-i1.stranded=$stranded ".
		"-i2.rsem_index=$rsem_index ".
		"-i2.read_strand=$read_strand ";
	}elsif($paired eq "Single-end"){
		$workflow = $se_r1_workflow; #ENCODE RNA-Seq (Long) Pipeline - 1 (single-end) replicate
		$cmd .= "dx run --brief $workflow --yes --destination=$results_path --name=$analysis_name $tagline".
		"-i0.reads=$r1r1_id ".
		"-i0.star_index=$star_index ".
		"-i1.chrom_sizes=$chrom_sizes ".
		"-i1.stranded=$stranded ".
		"-i2.rsem_index=$rsem_index ".
		"-i2.read_strand=$read_strand ";
	}
	print $cmd_fh "$cmd\n";

	#Setup for data download (dnanexus->LBL) and renaming / restructuring
	my $exp_path = "$local_path/RNA-seq/$pipeline_rid/$replicate/proc/$genome_dir";
	print $download_fh "#Experiment=$ex_id Replicate=$replicate\n";

	#rename alignment files
	my $fq_r1_r1_base = basename($fq_r1_r1); 
	$fq_r1_r1_base =~ s/\.fastq.gz//;
	my $fq_r1_r2_base = basename($fq_r1_r2); 
	$fq_r1_r2_base =~ s/\.fastq.gz//;
	my ($prefix);
	if($fq_r1_r1_base ne "NA" && $fq_r1_r2_base ne "NA"){
		$prefix = $fq_r1_r1_base."_".$fq_r1_r2_base;
	}else{
		$prefix = $fq_r1_r1_base;
	}
	print $download_fh "mv $exp_path/$prefix\_rsem.genes.results $exp_path/$replicate.gene_counts.tsv\n";
	print $download_fh "mv $exp_path/$prefix\_rsem.isoforms.results $exp_path/$replicate.isoform_counts.tsv\n";
	print $download_fh "mv $exp_path/$prefix\_star_Log.final.out $exp_path/$replicate.star_Log.final.out\n";
	print $download_fh "mv $exp_path/$prefix\_star_anno.bam $exp_path/$replicate.transcriptome.bam\n";
	print $download_fh "mv $exp_path/$prefix\_star_anno_flagstat.txt $exp_path/$replicate.transcriptome_mapping_stats.txt\n";
	print $download_fh "mv $exp_path/$prefix\_star_genome.bam $exp_path/$replicate.genome.bam\n";
	print $download_fh "mv $exp_path/$prefix\_star_genome_flagstat.txt $exp_path/$replicate.genome_mapping_stats.txt\n";
	if($stranded eq "true"){
		print $download_fh "mv $exp_path/$prefix\_star_genome_plusAll.bw $exp_path/$replicate.genome_plusAll.bw\n";
		print $download_fh "mv $exp_path/$prefix\_star_genome_plusUniq.bw $exp_path/$replicate.genome_plusUniq.bw\n";
		print $download_fh "mv $exp_path/$prefix\_star_genome_minusAll.bw $exp_path/$replicate.genome_minusAll.bw\n";
		print $download_fh "mv $exp_path/$prefix\_star_genome_minusUniq.bw $exp_path/$replicate.genome_minusUniq.bw\n";
	}else{
		print $download_fh "mv $exp_path/$prefix\_star_genome_all.bw $exp_path/$replicate.genome_all.bw\n";
		print $download_fh "mv $exp_path/$prefix\_star_genome_uniq.bw $exp_path/$replicate.genome_uniq.bw\n";
	}
	print $download_fh "\n";
	
	#Tidy up
}
#login to dnanexus
#system("dx logout") == 0 or die $!;


sub read_json{
	my ($rna_seq_json) = @_;
	my @rna_seq_args;

	#read json as array of hashes. One array element = a fastq with associated metadata
	my $all_metadata = json_file_to_perl ($rna_seq_json);
	my @all_metadata = @{$all_metadata};

	#Get a unique list of RNA-seq replicate IDs
	my @rnaseq_replicates = uniq(map {$_->{replicate}} @all_metadata);

	foreach my $replicate(@rnaseq_replicates){
		#get the json elements for each fastq for this replicate (info is duplicated apart from read number and filename)
		my ($fastq1_dat, $fastq1_file) = parse_fastq($replicate, 1, \@all_metadata);
		my ($fastq2_dat, $fastq2_file) = parse_fastq($replicate, 2, \@all_metadata);
		my %fastq1_dat = %{$fastq1_dat};
		my @args = ($fastq1_dat{dataset}, $fastq1_dat{experiment}, $replicate, $fastq1_dat{species}, $fastq1_dat{paired}, $fastq1_dat{stranded}, $fastq1_file, $fastq2_file);
                push(@rna_seq_args, \@args);
		
	}return \@rna_seq_args;

}

sub parse_fastq{
	my ($replicate, $strand, $metadata) = @_;
	my @all_metadata = @{$metadata};
	my (%fastq_dat, $fastq_file);
	my @fastq = grep {$_->{replicate} eq $replicate && $_->{read} == $strand} @all_metadata;
	if(@fastq > 0){
		%fastq_dat = %{$fastq[0]};		
		$fastq_file = $fastq_dat{filename};
	}else{
		$fastq_file = "NA";
	}
	return(\%fastq_dat, $fastq_file);
}
