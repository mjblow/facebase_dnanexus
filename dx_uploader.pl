#!/usr/bin/env perl
use warnings;
use strict;

#source activate facebase
#must be logged in to DNANexus using an API token

unless(@ARGV == 2){
	die "$0 <FaceBase_accession> <results_path>\n";
}

my $accession = $ARGV[0];
my $path = $ARGV[1];
my $local_path = "$path/data/$accession";
unless(-d("$local_path")){
	die "$local_path does not exist to upload!\n";
}

#Select the FaceBase project
my $select_proj = `dx select project-FF7xxXQ0jK8Fq5Kf4XzvFKf8`;

#Upload the folder
my $upload = `dx upload --wait --brief -r $local_path`;
