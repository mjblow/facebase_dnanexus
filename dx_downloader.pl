#!/usr/bin/env perl
use warnings;
use strict;
use File::Basename;

#source activate facebase
#must be logged in to DNANexus using an API token

unless(@ARGV == 1){
	die "$0 <FaceBase_accession>\n";
}

my $accession = $ARGV[0];
my $scratch =  "/global/projectb/scratch/mjblow";
my $local_path = "$scratch/facebase_dnanexus/data/$accession";
unless(-d("$local_path")){
	die "Can't find dataset with ID: $accession to upload!\n";
}

#Select the FaceBase project
my $select_proj = `/global/u2/m/mjblow/.conda/envs/facebase/bin/dx select project-FF7xxXQ0jK8Fq5Kf4XzvFKf8`;

#Download the results folder
my $download_chipseq = `/global/u2/m/mjblow/.conda/envs/facebase/bin/dx download -f -r -o $local_path $accession/ChIP-seq`;

my $download_rnaseq = `/global/u2/m/mjblow/.conda/envs/facebase/bin/dx download -f -r -o $local_path $accession/RNA-seq`;

#Flatten ChIP-seq analysis results
my @enc = glob("$local_path/ChIP-seq/*/*/*/*/*/encode*");
foreach my $enc(@enc){
	my $to = dirname($enc);
	my $flatten = `mv $enc/* $to/.; rm -rf $enc`;
}
