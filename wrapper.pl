#!/usr/bin/env perl
use warnings;
use strict;

unless(@ARGV == 3){
	die "usage: $0 <FaceBase_dataset> <results_path> <server>\n";
}

my $dataset = $ARGV[0];
my $path = $ARGV[1];
my $server = $ARGV[2];

#Run this wrapper script on one of the data transfer nodes (e.g. dtn02.nersc.gov);
#Add these lines to ~/.cshrc.ext: 
#set path=($path /global/projectb/sandbox/RD/mjblow/envs/facebase/bin)
#set path=($path /global/projectb/sandbox/RD/mjblow/envs/facebase/bin/perl)
#Example command:
#nohup perl wrapper.pl FB00000806.2 /global/projectb/scratch/mjblow/facebase_dnanexus staging.facebase.org > & FB00000806.2.log &

###Step 1. Download data from FaceBase
system("perl facebase_download.pl $dataset $path $server") == 0 or die $!; #      **** OK! *****

###Step 2. Upload data to DNA Nexus
system("perl dx_uploader.pl $dataset $path") == 0 or die $!; #      **** OK! *****

###Step 3. Setup ChIP-Seq analyses for dataset
my $n_chip = `/global/projectb/sandbox/RD/mjblow/envs/facebase/bin/perl dx_chipseq_analysis.pl $dataset $path $server`;  #      **** LIKELY OK! *****

###Step 4. Setup RNA-Seq analyses for dataset
my $n_rna = `/global/projectb/sandbox/RD/mjblow/envs/facebase/bin/perl dx_rnaseq_analysis.pl $dataset $path $server`;  #      **** LIKELY OK! *****

###Step 5. Submit the analyses to DNANexus
system("touch $dataset.rnaseq.ids.txt $dataset.chipseq.ids.txt") == 0 or die$!;
if($n_rna > 0){
	system("sh $dataset.rnaseq.dx_submit.sh > $dataset.rnaseq.ids.txt") == 0 or die $!;
}
if($n_chip > 0){
	system("sh $dataset.chipseq.dx_submit.sh > $dataset.chipseq.ids.txt ") == 0 or die $!;
}

#wait for jobs to finish on dnanexus 
system("cat $dataset.rnaseq.ids.txt $dataset.chipseq.ids.txt | xargs dx wait") == 0 or die$!;

###Step 6. Download the results from DNANexus 
if($n_rna > 0){
	system("sh $dataset.rnaseq.dx_results_download.sh") == 0 or die $!;
}
if($n_chip > 0){
	system("sh $dataset.chipseq.dx_results_download.sh") == 0 or die $!;
}

###Step 7. Upload the results to FaceBase 
system("deriva-upload-cli  --credential-file ~/.deriva/credential.json $server /projectb/scratch/mjblow/facebase_dnanexus/data/$dataset") == 0 or die $!;

###Step 8. Get the price of the analysis
system("perl dx_price.pl $dataset > $dataset.price.txt") == 0 or die $!;
