#!/usr/bin/env perl
use warnings;
use strict;
use File::Basename;

#source activate facebase
#must be logged in to DNANexus using an API token

unless(@ARGV == 1){
	die "$0 <FaceBase_accession>\n";
}

my $accession = $ARGV[0];

#get analysis ids
my @analysis_ids = `dx find analyses -n 10000 --brief --tag dataset=$accession`;
my $total;
foreach my $id(@analysis_ids){
	$id =~ s/\n//;
	my $price_line = `dx describe $id | grep \"Total Price"`;
	my $price;
	if($price_line =~ /.*\$(.*)$/){
		$price = $1;
	}
	print "price for $id = $price\n";
	$total += $price;
}
print "\nTotal cost for $accession = $total\n";
