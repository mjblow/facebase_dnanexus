#!/usr/bin/env perl
use warnings;
use strict;

# SETUP:
# source activate facebase unless on dtn03
# obtain <token> by launching DERIVA-Auth app on Mac Laptop

unless(@ARGV == 3){

	die "$0 <FaceBase_accession> <results_path> <server>\n";

	#die "$0 <FaceBase_accession> <DERIVA-Auth token>\n"; # Don't need to specify token on command line once  ~/.deriva/credential.json has been setup!
}
my $accession = $ARGV[0];
my $path = $ARGV[1];
my $server = $ARGV[2];

#Download the data
my $cmd = "deriva-download-cli --catalog 1 $server fb-seq-bag.json $path dataset=$accession";

#print $cmd; exit;

system($cmd) == 0 or die $!;

#Reorganize the files
unless(-d("$path/data/$accession/fastq")){
	system("mkdir -p $path/data/$accession/fastq") == 0 or die$!;
}
unless(-d("$path/json")){
	system("mkdir $path/json") == 0 or die$!;
}
unless(-d("$path/logs")){
	system("mkdir $path/logs") == 0 or die$!;
}
system("mv $path/FaceBase-Seq-Bag/data/$accession/*json  $path/json/.") == 0 or die $!;
system("find $path/FaceBase-Seq-Bag/data/$accession -mindepth 2 -type f -exec mv -t $path/data/$accession/fastq/ -i '{}' +") == 0 or die $!;
system("tar -zcf $path/logs/$accession\_download.tar.gz $path/FaceBase-Seq-Bag") == 0 or die $!;
system("rm -rf $path/FaceBase-Seq-Bag") == 0 or die $!;

